#!/usr/bin/env bash

rm -rf public

hugo

cp .domains public

git add .
git commit -a -m'deploy'
git push

git subtree push --prefix=public git@codeberg.org:lesautres/manifeste.git pages
